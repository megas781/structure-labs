package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
	// write your code here

        MassivTest arr = new MassivTest(10);

        arr.insert(5);
        arr.insert(3);
        arr.insert(8);
        arr.insert(2);
        arr.insert(7);
        arr.insert(1);
        arr.insert(new Random().nextInt(10));
        System.out.println("До преобразования:");
        arr.display();

        arr.delete(3);
        arr.delete(1);
        arr.delete(5);
        arr.delete(2);
        arr.delete(7);
        arr.delete(8);
        System.out.println("после");
        arr.display();

//        int arg = 5;
//
//        if (arr.find(7)) {
//            System.out.println("Значение " + arg + " действительно есть в массиве");
//        } else {
//            System.out.println("Значение " + 7 + " отстутствует");
//        }
//
//        System.out.println("");
//
//        arr.delete(arg);
//
//        System.out.println("После преобразования");
//        arr.display();
//
//        if (arr.find(arg)) {
//            System.out.println("Значение " + arg + " действительно есть в массиве");
//        } else {
//            System.out.println("Значение " + arg + " отстутствует");
//        }

    }
}

