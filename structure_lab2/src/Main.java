import java.lang.reflect.Array;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // write your code here


        //Создание массива
        int[] hugeArray = new int[2500];
        for (int i = 0; i < hugeArray.length; i++) {
            hugeArray[i] = new Random().nextInt(2500);
        }



//        Сортировка выбором
        int[] selectSort = hugeArray.clone();

        long startSelect = System.currentTimeMillis();

        for (int i = 0; i < selectSort.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < selectSort.length; j++) {
                if (selectSort[j] < selectSort[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = selectSort[i];
            selectSort[i] = selectSort[minIndex];
            selectSort[minIndex] = temp;
        }

        long selectDuration = System.currentTimeMillis() - startSelect;



        System.out.println("Сортировка выбором (" + selectDuration + " мс):");
        for (int i = 0; i < selectSort.length; i++) {
            System.out.print(selectSort[i] + " ");
        }

        System.out.println("\n");


        long startInsert = System.currentTimeMillis();

//        Сортировка вставками
        int[] insertSort = hugeArray.clone();
        for (int i = 1; i < insertSort.length; i++) {

            int minValue = insertSort[i];
            int prevIndex = i - 1;

            while (prevIndex >= 0 && insertSort[prevIndex] > minValue) {
                insertSort[prevIndex + 1] = insertSort[prevIndex];
                prevIndex -= 1;
            }
            insertSort[prevIndex + 1] = minValue;
        }

        long durationInsert = System.currentTimeMillis() - startInsert;

        System.out.println("Сортировка вставками (" + durationInsert + " мс):");
        for (int i = 0; i < insertSort.length; i++) {
            System.out.print(insertSort[i] + " ");
        }


    }


}
