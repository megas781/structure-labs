import java.util.Deque;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here

        Scanner stdin = new Scanner(System.in);

        System.out.println("Введите выражение:");
        String input = stdin.nextLine();

//        System.out.println("Прочитал: \"" + input + "\"");

        String[] units = input.split(" ");

        for (int i = 0; i < units.length; i++) {
            System.out.println("part " + i + ": " + units[i]);
        }


        Stack<Double> numbers = new Stack<Double>();


        for (int i = 0; i < units.length; i++) {
            try {

                double number = Double.parseDouble(units[i]);

                numbers.push(number);

                System.out.println("число: " + number);

            } catch (NumberFormatException e) {

//                Если выпала ошибка, значит перед нами знак

                //Достаём данные именно в таком порядке
                double arg2 = numbers.pop();
                double arg1 = numbers.pop();

                switch (units[i]) {
                    case "+":
                        numbers.push(arg1 + arg2);
                        break;
                    case "-":
                        numbers.push(arg1 - arg2);
                        break;
                    case "*":
                        numbers.push(arg1 * arg2);
                        break;
                    case "/":
                        numbers.push(arg1 / arg2);
                        break;
                    default:
//                        Здесь должен быть FatalError
                        System.out.println("default??");
                }

                System.out.println("now stack: " + numbers.peek());

            }
            System.out.println("");
        }


        System.out.println("Ответ: " + numbers.pop());


    }
}
